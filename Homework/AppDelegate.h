//
//  AppDelegate.h
//  Homework
//
//  Created by Michael Junez Avila on 10/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

