//
//  Value.h
//  Homework
//
//  Created by Michael Junez Avila on 9/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONMOdel.h"
#import "Schedule.h"

@interface Venue : JSONModel

@property (strong, nonatomic) NSString *zip;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *ticket_link;
@property (strong, nonatomic) NSString *state;
@property short pcode;
@property (strong, nonatomic) NSString *city;
@property short venueId;
@property (strong, nonatomic) NSString *tollfreephone;
@property (strong, nonatomic) NSArray<Schedule> *schedule;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *image_url;
@property (strong, nonatomic) NSString *Description;
@property (strong, nonatomic) NSString *name;
@property float longitude;
@property float latitude;

@end
