//
//  Schedule.h
//  Homework
//
//  Created by Michael Junez Avila on 9/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONMOdel.h"

@protocol Schedule
@end

@interface Schedule : JSONModel

@property (strong, nonatomic) NSString *end_date;
@property (strong, nonatomic) NSString *start_date;

@end
