//
//  StringUtils.m
//  Homework
//
//  Created by Michael Junez Avila on 9/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import "LocalDateFormatter.h"

@implementation LocalDateFormatter

+ (NSString *)getScheduleStringFromArray: (NSArray *) scheduleArray {
    
    NSString *dateString1 = (NSString *)[scheduleArray objectAtIndex:0];
    NSString *dateString2 = (NSString *)[scheduleArray objectAtIndex:1];
    
    // convert to date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"YYYY-MM-dd' 'HH:mm:ss' -0800'"];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    
    NSDate *dte1 = [dateFormat dateFromString:dateString1];
    NSDate *dte2 = [dateFormat dateFromString:dateString2];    
    
    //convert to new string
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE M/DD"];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm a"];
    
    NSString *scheduleFormatted = [NSString stringWithFormat:@"%@ %@ to %@\n", [[dateFormatter stringFromDate:dte1] capitalizedString], [timeFormatter stringFromDate:dte1], [timeFormatter stringFromDate:dte2]];
    
    return scheduleFormatted;
}

@end
