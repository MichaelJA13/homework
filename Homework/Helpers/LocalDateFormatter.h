//
//  StringUtils.h
//  Homework
//
//  Created by Michael Junez Avila on 9/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalDateFormatter : NSObject

+ (NSString *)getScheduleStringFromArray: (NSArray *) scheduleArray;

@end
