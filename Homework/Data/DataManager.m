//
//  DataManager.m
//  Homework
//
//  Created by Michael Junez Avila on 9/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import "DataManager.h"
#import "Venue.h"
#import <AFNetworking/AFNetworking.h>

@implementation DataManager

@synthesize delegate;

-(void)getVenues{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:@"https://s3.amazonaws.com/jon-hancock-phunware/nflapi-static.json" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getVenuesFinishedSuccesfully:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [delegate getVenuesFailedWithError:error];
    }];
}

-(void)getVenuesFinishedSuccesfully:(NSData *)data{
    
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    NSMutableArray *venues = [Venue arrayOfModelsFromDictionaries:(NSArray *)parsedObject];
    
    [delegate getVenuesFinishedSuccesfully:venues];
}

@end
