//
//  DataManager.h
//  Homework
//
//  Created by Michael Junez Avila on 9/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataManagerDelegate <NSObject>

- (void)getVenuesFinishedSuccesfully:(NSMutableArray *)venuesArray;
- (void)getVenuesFailedWithError:(NSError *)error;

@end

@class DataCommunicator;

@interface DataManager : NSObject
@property (weak, nonatomic) id<DataManagerDelegate> delegate;

-(void)getVenues;

@end
