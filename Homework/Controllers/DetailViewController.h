//
//  DetailViewController.h
//  Homework
//
//  Created by Michael Junez Avila on 10/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../Model/Venue.h"

@interface DetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) Venue *detailItem;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITableView *scheduleTableView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

