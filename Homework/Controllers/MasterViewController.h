//
//  MasterViewController.h
//  Homework
//
//  Created by Michael Junez Avila on 10/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController<DataManagerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DataManager *dataManager;
@property (strong, nonatomic) NSMutableArray *venues;
@property (strong, nonatomic) IBOutlet UITableView *venuesTableView;



@end

