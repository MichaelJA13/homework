//
//  MasterViewController.m
//  Homework
//
//  Created by Michael Junez Avila on 10/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#include "TargetConditionals.h"
#import "DataManager.h"
#import "VenueCellController.h"
#import "Venue.h"

@interface MasterViewController ()

@property NSMutableArray *objects;
@end

@implementation MasterViewController

@synthesize dataManager, venues, venuesTableView;

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    
    dataManager = [[DataManager alloc] init];
    dataManager.delegate = self;
    [dataManager getVenues];
    
    [venuesTableView setDataSource:self];
    [venuesTableView setDelegate:self];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Getting Data

- (void)getVenuesFinishedSuccesfully:(NSMutableArray *)venuesArray{
    self.venues = venuesArray;
    [venuesTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}
- (void)getVenuesFailedWithError:(NSError *)error{}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Venue *venue = self.venues[indexPath.row];
        
        #if !(TARGET_IPHONE_SIMULATOR)
            DetailViewController *controller = (DetailViewController *)[segue destinationViewController];
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            {
                controller = (DetailViewController *)[[segue destinationViewController] topViewController];
            }
        #else
            DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        #endif
        
        [controller setDetailItem:venue];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.venues.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VenueCellController *cell = (VenueCellController *)[tableView dequeueReusableCellWithIdentifier:@"venueCell"];
    Venue *venue = [venues objectAtIndex:indexPath.row];
    cell.nameLabel.text = venue.name;
    cell.addressLabel.text = [NSString stringWithFormat:@"%@, %@, %@ %@", venue.address, venue.city, venue.state, venue.zip];
    return cell;}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

@end
