//
//  VenueCellController.h
//  Homework
//
//  Created by Michael Junez Avila on 9/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueCellController : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *addressLabel;

@end
