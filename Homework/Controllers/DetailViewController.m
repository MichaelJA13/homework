//
//  DetailViewController.m
//  Homework
//
//  Created by Michael Junez Avila on 10/4/15.
//  Copyright (c) 2015 MJA. All rights reserved.
//

#import "UIImageView+WebCache.h"
#import "DetailViewController.h"
#import "LocalDateFormatter.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.nameLabel.text = [self.detailItem name];
        self.addressLabel.text = [NSString stringWithFormat:@"\n%@\n%@, %@ %@\n", self.detailItem.address, self.detailItem.city, self.detailItem.state, self.detailItem.zip];
        
        [self.scheduleTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        
        if (!self.detailItem.image_url || [self.detailItem.image_url isEqualToString:@""] )
            [self.imageView setImage:[UIImage imageNamed:@"image_placeholder"]];
        else
            [self.imageView sd_setImageWithURL:[NSURL URLWithString:self.detailItem.image_url] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.nameLabel setText:@""];
    [self.addressLabel setText:@""];
    
    [self.scheduleTableView setDataSource:self];
    [self.scheduleTableView  setDelegate:self];
    
    [self configureView];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.detailItem.schedule count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"scheduleCell"];

    Schedule *schedule = [self.detailItem.schedule objectAtIndex:indexPath.row];
    
    UILabel *scheduleLabel = (UILabel*) [cell viewWithTag:1];
    
    scheduleLabel.text = [LocalDateFormatter getScheduleStringFromArray:@[schedule.start_date, schedule.end_date]];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
